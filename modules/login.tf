provider "azurerm" {
  # Whilst version is optional, we /strongly recommend/ using it to pin the version of the Provider being used
  version = "=1.44.0"

  subscription_id = "b86f0719-57a6-48fd-ac4c-9003668c3a97"
  tenant_id       = "c598db1f-a4b2-4381-b551-a15e6abf54d5"
}